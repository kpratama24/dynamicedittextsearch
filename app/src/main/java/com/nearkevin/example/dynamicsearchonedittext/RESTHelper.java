package com.nearkevin.example.dynamicsearchonedittext;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RESTHelper {

    private static Retrofit retrofit;
    private static final String API_BASE_URL="https://nearkevin.com/ngelesin/api/";

    /**
     * Method to get the required retrofit instance
     * @return retrofit instance
     */
    private static Retrofit getRetrofitInstance(){
        if(retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private static GetPostDataService getService(){
        GetPostDataService service;
        service = getRetrofitInstance().create(GetPostDataService.class);
        return service;
    }
}
