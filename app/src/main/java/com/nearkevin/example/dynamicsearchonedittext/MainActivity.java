package com.nearkevin.example.dynamicsearchonedittext;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    List<String> listKota;
    SearchResultAdapter searchResultAdapter;
    @BindView(R.id.edit_search_query)
    EditText editSearchQuery;
    @BindView(R.id.recycler_search_result)
    RecyclerView recyclerSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listKota = new ArrayList<>();
        listKota.add("Jakarta");
        listKota.add("Bandung");
        listKota.add("Bogor");
        listKota.add("Cimahi");
        listKota.add("Cirebon");
        listKota.add("Tasikmalaya");
        listKota.add("Tanjungpinang");
        listKota.add("Bandar Lampung");
        listKota.add("Metro");
        listKota.add("Ternate");
        listKota.add("Kendari");
        listKota.add("Bukittinggi");
        listKota.add("Padang");
        listKota.add("Padangpanjang");
        listKota.add("Yogyakarta");
        listKota.add("Sibolga");
        listKota.add("Medan");


        setKota(listKota);

        editSearchQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String query = s.toString();

                List<String> searchMatch = new ArrayList<>();

                for(int i = 0; i < listKota.size(); i++){
                    if(listKota.get(i).contains(query)){
                        searchMatch.add(listKota.get(i));
                    }
                }

                setKota(searchMatch);
            }
        });
    }

    private void setKota(List<String> listKota) {

        searchResultAdapter = new SearchResultAdapter(listKota, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerSearchResult.setLayoutManager(layoutManager);
        recyclerSearchResult.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerSearchResult.setAdapter(searchResultAdapter);
    }
}
