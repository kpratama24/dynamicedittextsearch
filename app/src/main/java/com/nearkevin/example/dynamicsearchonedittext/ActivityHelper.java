package com.nearkevin.example.dynamicsearchonedittext;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

public class ActivityHelper {

    /**
     * Helper for getting specific shared preferences
     * @param act the current activity
     * @param name the preferences key name
     * @return the shared preference instances.
     */
    public static SharedPreferences getSharedPreferences(Activity act,String name){
        SharedPreferences sharedPref = act.getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedPref;
    }

    /**
     * Write the shared preferences
     * @param act current activity
     * @param preferences the preferences key name
     * @param name the preferences name
     * @param value the preferences value
     */
    public static void writeSharedPreferences(Activity act, String preferences, String name,
                                              String value){
        SharedPreferences sharedPref = act.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(name,value);
        editor.apply();
    }

    /**
     * Clear selected preferences
     * @param act current activity
     * @param preferences preferences key
     * @param removed the shared preferences name to be removed. if leaves empty, clear all
     *                preferences
     */
    public static void clearSharedPreferences(Activity act, String preferences,
                                              @Nullable String removed){
        SharedPreferences sharedPref = act.getSharedPreferences(preferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if(removed==null){
            editor.clear().apply();
        }
        else editor.remove(removed).apply();
    }
}
