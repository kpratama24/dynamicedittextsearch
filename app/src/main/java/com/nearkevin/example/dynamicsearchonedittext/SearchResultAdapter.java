package com.nearkevin.example.dynamicsearchonedittext;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {

    private List<String> searchResults;
    private Context ctx;

    private RESTHelper rRest;

    public SearchResultAdapter(List<String> orderLists, Context ctx) {
        this.searchResults = orderLists;
        this.ctx = ctx;
        rRest = new RESTHelper();
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_search_result, parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        holder.textNamaKota.setText(searchResults.get(position));
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    public class SearchResultViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        @BindView(R.id.text_nama_kota)
        TextView textNamaKota;

        public SearchResultViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            ButterKnife.bind(this, itemView);
        }


    }
}
